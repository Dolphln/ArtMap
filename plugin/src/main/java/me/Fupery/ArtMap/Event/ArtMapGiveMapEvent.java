package me.Fupery.ArtMap.Event;

import me.Fupery.ArtMap.Utils.ItemUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Called when a map is saved by a player
 */
public class ArtMapGiveMapEvent extends PlayerEvent {

    private static final HandlerList handlers = new HandlerList();

    private final ItemStack mapItem;
    private final String title;

    public ArtMapGiveMapEvent(Player player, ItemStack mapItem, String title) {
        super(player);
        this.mapItem = mapItem;
        this.title = title;

        if (!Bukkit.getPluginManager().isPluginEnabled("ArtMapPlus")) {
            ItemUtils.giveItem(player, mapItem);
        }
    }

    public ItemStack getMapItem() {
        return this.mapItem;
    }

    public String getTitle() {
        return title;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
